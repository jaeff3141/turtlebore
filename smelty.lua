turtle.select(1)

function waitForCast()
    local time = 0
    while not turtle.suck() do
        if (time == 15) then
            return false
        end
        sleep(1)
        time = time + 1
    end
    turtle.dropDown()
    return true
end

while true do
    while true do
        redstone.setOutput("top", true)
        sleep(.2)
        redstone.setOutput("top", false)

        if not waitForCast() then
            break
        end
    end

    sleep(60)
end


