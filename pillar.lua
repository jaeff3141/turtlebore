os.loadAPI("utils")

function selectItem()
    for n=1,16 do
        if turtle.getItemCount(n) > 0 then
            turtle.select(n)
            return true
        end
    end
    return false
end

local location = utils.location()
local height = 1
while true do
    for n=1,3 do
        for m=1,3 do
            if not selectItem() then
                return
            end
            utils.moveTo({ location[1]+n, location[2]+height, location[3]+m })
            turtle.placeDown()
        end
    end
    height = height + 1
end


