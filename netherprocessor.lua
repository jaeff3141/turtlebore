args = { ... }
COMPRESSIBLE_FILE = ".compressible"

BLACKLIST = {}
-- compressed obsidian isn't reversible
BLACKLIST["minecraft:obsidian:0"] = true

TRASHLIST = {}
TRASHLIST["minecraft:netherrack:0"] = true

os.loadAPI("utils")

INPUT = FORWARD
TRASH = DOWN
OUTPUT = UP

-- forward = input
-- down = temp space
-- up = permanent storage

function loadCompressibleData()
    local f = fs.open(COMPRESSIBLE_FILE, "r")
    if not f then
        return {}
    end

    local data = f.readAll()
    f.close()

    if data == nil or #data == 0 then
        f.close()
        return {}
    end
    return textutils.unserialize(data)
end

function saveCompressibleData()
    local f, msg = fs.open(COMPRESSIBLE_FILE, "w")
    if not f then
        error(msg)
    end
    f.write(textutils.serialize(compressibleTable))
    f.close()
end

function isCompressible(blockName)
    if BLACKLIST[blockName] then
        return false
    end
    return compressibleTable[blockName]
end

function mightBeCompressible(blockName)
    if BLACKLIST[blockName] then
        return false
    end
    -- either nil or true
    return compressibleTable[blockName] ~= false
end

function setCompressible(blockName, compressible)
    compressibleTable[blockName] = compressible
    saveCompressibleData()
end

function getBlockName(n)
    return utils.getBlockName(turtle.getItemDetail(n))
end

function spreadItemsAndCraft()
    local itemCount = turtle.getItemCount()
    local spreadCount = math.floor(itemCount / 9)

    turtle.transferTo(2, spreadCount)
    turtle.transferTo(3, spreadCount)
    turtle.transferTo(5, spreadCount)
    turtle.transferTo(6, spreadCount)
    turtle.transferTo(7, spreadCount)
    turtle.transferTo(9, spreadCount)
    turtle.transferTo(10, spreadCount)
    turtle.transferTo(11, spreadCount)

    turtle.select(13)
    return turtle.craft()
end

function clearInventory(tempDirection)
    for n=1,16 do
        if turtle.getItemCount(n) > 0 then
            turtle.select(n)
            if mightBeCompressible(getBlockName(n)) then
                -- if it might be compressible, send it to temp storage
                utils.drop(tempDirection)
            else
                -- if it's definitely not compressible, send it to permanent storage
                turtle.dropUp()
            end
        end
    end
end

function compress()
    clearInventory(OUTPUT)

    while true do
        turtle.select(1)
        if not utils.suck(INPUT) then
            break
        end

        local blockName = getBlockName(1)

        if TRASHLIST[blockName] then
            utils.drop(TRASH)
        elseif turtle.getItemCount() >= 9 then
            local compressible = isCompressible(blockName)
            if compressible ~= false then
                local result = spreadItemsAndCraft()
                if compressible == nil then
                    setCompressible(blockName, result)
                end
            end
        end
        clearInventory(OUTPUT)
    end
end

compressibleTable = loadCompressibleData()

while true do
    print("compressing...")
    compress()
    print("waiting...")
    sleep(60)
end