args = { ... }

TARGET_ENERGY = 5000000

function usage()
    print("Usage: reactorizor <initial_insertion> <min_insertion> <max_insertion>")
end

if #args ~= 3 then
    usage()
    return
end

initial_insertion = tonumber(args[1])
min_insertion = tonumber(args[2])
max_insertion = tonumber(args[3])


if initial_insertion == nil or min_insertion == nil or max_insertion == nil then
    usage()
    return
end

br = peripheral.wrap("left")
if not br then
    br = peripheral.wrap("right")
end

if not br then
    print("Could not find reactor control port")
    return
end

energy_generation = {}

estimated_energy_use = nil
previous_energy_production = br.getEnergyProducedLastTick()
previous_energy_level = br.getEnergyStored()
current_energy_production = br.getEnergyProducedLastTick()
current_energy_level = br.getEnergyStored()


function update_values()
    previous_energy_level = current_energy_level
    previous_energy_production = current_energy_production
    current_energy_production = br.getEnergyProducedLastTick()
    current_energy_level = br.getEnergyStored()
    print(string.format("production=%d, level=%d", current_energy_production, current_energy_level))
end

function waitForEquilibrium()
    print("waiting for equilibrium")
    while true do
        sleep(5)
        update_values()

        if math.abs(((previous_energy_production - current_energy_production) / current_energy_production)) < .01 then
            energy_generation[br.getControlRodLevel(1)] = current_energy_production
            return
        end
    end
    print("equilibrium achieved")
end

function overproduction()
    if current_energy_level > 9800000 then
        return true
    end

    return current_energy_level - previous_energy_level > 0
end

function overstorage()
    return current_energy_level > 5000000
end

function underproduction()
    if current_energy_level < 200000 then
        return true
    end

    return current_energy_level - previous_energy_level < 0
end

function understorage()
    return current_energy_level < 5000000
end

function setRods(insertion)
    if insertion > max_insertion then
        insertion = max_insertion
    elseif insertion < min_insertion then
        insertion = min_insertion
    end
    print(string.format("Setting control rods to %d", insertion))
    br.setAllControlRodLevels(insertion)
end

function getRods()
    return br.getControlRodLevel(1)
end

function estimate_energy_use()
    local estimated_energy_use = ((previous_energy_level - current_energy_level) / (5*20)) + ((previous_energy_production + current_energy_production)/2)
    print("estimated energy use: " .. tostring(estimated_energy_use))
end


setRods(initial_insertion)
waitForEquilibrium()

while true do
    update_values()

    if current_energy_level < 9800000 and current_energy_level > 200000 then
        estimate_energy_use()
    end

    if overproduction() then
        print("over production")
        if overstorage() then
            print("over storage")
            setRods(getRods() + 1)
            waitForEquilibrium()
        elseif (5000000 - current_energy_level) / (current_energy_level - previous_energy_level) < 2 then
            print("energy rising too fast")
            setRods(getRods() + 1)
            waitForEquilibrium()
        end
    elseif underproduction() then
        print("under production")
        if understorage() then
            print("under storage")
            setRods(getRods() - 1)
            waitForEquilibrium()
        elseif (5000000 - current_energy_level) / (current_energy_level - previous_energy_level) < 2 then
            print("energy level dropping too fast")
            setRods(getRods() - 1)
            waitForEquilibrium()
        end
    end

    sleep(5)
end