while true do
    for n=1,16 do
        if turtle.getItemCount(n) > 0 then
            turtle.select(n)
        end

        while turtle.getItemCount(n) > 0 do
            if not turtle.detect() then
                turtle.place()
            end
            sleep(.1)
        end
    end

    os.pullEvent("turtle_inventory")
end

