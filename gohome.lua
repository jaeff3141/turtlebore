local f, msg = fs.open(".gohome", "w")
if not f then
    error(msg)
end

f.write("gohome")
f.close()

while true do
    print("Press R to resume mining")

    local event, key = os.pullEvent("key")

    if key == keys.r then
        print("Resuming")
        fs.delete(".gohome")
        break
    end
end