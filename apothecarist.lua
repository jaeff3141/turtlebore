-- up - chest with lock item, and items to drop into the apothecary
-- down - infinite water source
-- right - comparator from apothecary
-- forward -- chest with bucket and seeds

function inventoryEmpty()
    for n=1,16 do
        if turtle.getItemCount(n) > 0 then
            return false
        end
    end
    return true
end

function getInput(side)
    local value = redstone.getAnalogInput(side)
    print(string.format("got redstone=%d", value))
    return value
end

while true do
    print("Waiting for inventory.")
    os.pullEvent("turtle_inventory")

    print("Inventory changed. We're up!")

    if not inventoryEmpty() then
        print("Yep, got something!")

        -- grab the arbitrary lock object. This ensures that the interface can't push any more items
        turtle.select(16)
        turtle.suckUp()

        -- grab the empty bucket and seeds
        turtle.suck()
        turtle.suck()

        for n=1,15 do
            if turtle.getItemCount(n) > 0 then
                if turtle.getItemDetail(n)["name"] == "minecraft:bucket" then
                    print("Filling apothecary with water")
                    turtle.select(n)
                    turtle.placeDown()
                    turtle.dropUp()
                    break
                end
            end
        end

        print("Waiting for initial redstone signal")
        while getInput("right") == 0 do
            os.pullEvent("redstone")
        end

        print("Sending the rest of the items")
        for n=1,15 do
            if turtle.getItemCount(n) > 0 then
                turtle.select(n)
                if turtle.getItemDetail(n)["name"] == "minecraft:wheat_seeds" then
                    if not turtle.dropUp(1) then
                        print("hmm. couldn't drop the items")
                    end
                    turtle.drop()
                else
                    if not turtle.dropUp() then
                        print("hmm. couldn't drop the items")
                    end
                end
            end
        end

        print("Waiting for completion")
        while getInput("right") > 0 do
            os.pullEvent("redstone")
        end

        print("Got completion, finishing up")
        turtle.select(16)
        turtle.dropUp()
    else
        print("Aw crud, there's nothing to do.")
    end
end

