if IN_GAME == nil then
    IN_GAME = true
end

if not IN_GAME then
    turtle = {}
    fs = {}

    function turtle.up()
        log("up")
        return true
    end

    function turtle.down()
        log("down")
        return true
    end

    function turtle.forward()
        log("forward")
        return true
    end

    function turtle.turnRight()
        log("turnRight")
    end

    function turtle.turnLeft()
        log("turnLeft")
    end

    function turtle.dig()
        log("dig")
        return true
    end

    function turtle.digUp()
        log("digUp")
        return true
    end

    function turtle.digDown()
        log("digDown")
        return true
    end

    function turtle.drop()
    end

    function turtle.dropUp()
    end

    function turtle.dropDown()
    end

    function turtle.attack()
    end

    function turtle.attackUp()
    end

    function turtle.attackDown()
    end

    function turtle.detect()
    end

    function turtle.detectUp()
    end

    function turtle.detectDown()
    end

    function turtle.suck()
    end

    function turtle.suckUp()
    end

    function turtle.suckDown()
    end

    function turtle.getItemCount()
        return 0
    end

    function turtle.getFuelLevel()
        return math.huge
    end

    function turtle.select()
    end

    function turtle.refuel()
        return true
    end

    function fs.exists()
    end

    function fs.delete()
    end
end

math.randomseed(os.time())
fs.delete("log")
function log(...)
    local msg = string.format(unpack(arg))

    if IN_GAME then
        local f, err = fs.open("log", "a")
        if not f then
            error(err)
        end

        f.writeLine(msg)
        f.close()
    end

    print(msg)
end

local HOME_FILE = ".home"
local LOCATION_FILE = ".location"

local constants = {}
setmetatable(constants, {__newindex = function(table, key, value)
    rawset(table, key, value)
    _G[key] = value
end
})

constants.SOUTH = 0 -- positive z
constants.WEST = 1 -- negative x
constants.NORTH = 2 -- negative z
constants.EAST = 3 -- positive x

constants.FORWARD = 0
constants.RIGHT = 1
constants.BACKWARD = 2
constants.LEFT = 3
constants.UP = 4
constants.DOWN = 5

constants.X = 1
constants.Y = 2
constants.Z = 3

constants.POSITIVE = 1
constants.NEGATIVE = -1

local DIRECTION_NAMES = {}
DIRECTION_NAMES[SOUTH] = "SOUTH"
DIRECTION_NAMES[WEST] = "WEST"
DIRECTION_NAMES[NORTH] = "NORTH"
DIRECTION_NAMES[EAST] = "EAST"
DIRECTION_NAMES[UP] = "UP"
DIRECTION_NAMES[DOWN] = "DOWN"

local movementVectors = {}
movementVectors[SOUTH] = {0, 0, 1}
movementVectors[WEST] = {-1, 0, 0}
movementVectors[NORTH] = {0, 0, -1}
movementVectors[EAST] = {1, 0, 0}
movementVectors[UP] = {0, 1, 0}
movementVectors[DOWN] = {0, -1, 0}

local private = {}

function addVector(vec1, vec2)
    return {vec1[1] + vec2[1],
        vec1[2] + vec2[2],
        vec1[3] + vec2[3]}
end

local function multiplyComponent(component, scalar)
    -- workaround for 0 * math.huge == nan
    if component == 0 or scalar == 0 then
        return 0
    end
    return component * scalar
end

function multiplyVector(vec, scalar)
    return {multiplyComponent(vec[1], scalar),
        multiplyComponent(vec[2], scalar),
        multiplyComponent(vec[3], scalar)}
end

function init(table)
    for name, value in pairs(constants) do
        table[name] = value
    end
end

function location()
    -- we don't want to return the original table
    return {state.location[1], state.location[2], state.location[3]}, state.heading
end

function home()
    if not fs.exists(HOME_FILE) then
        return {0, 0, 0}, SOUTH
    end

    local f, msg = fs.open(HOME_FILE, "r")

    local x = tonumber(f.readLine())
    local y = tonumber(f.readLine())
    local z = tonumber(f.readLine())
    local heading = tonumber(f.readLine())
    local blockName = f.readLine()
    if blockName == "nil" then
        blockName = nil
    end
    f.close()

    return {x, y, z}, heading, blockName
end

function setHome(coord, heading)
    changeHeading((heading + 2) % 4)
    local success, blockDetail = turtle.inspect()
    changeHeading(heading)

    local blockName
    if success then
        blockName = getBlockName(blockDetail)
    end

    local f, msg = fs.open(HOME_FILE, "w")
    if not f then
        error(msg)
    end

    f.writeLine(tostring(coord[X]))
    f.writeLine(tostring(coord[Y]))
    f.writeLine(tostring(coord[Z]))
    f.writeLine(tostring(heading))
    f.writeLine(tostring(blockName))
    f.close()
end

function private.compareCoords(coord1, coord2)
    for n=1,3 do
        if coord1[n] ~= coord2[n] then
            return false
        end
    end
    return true
end

function relativeMovementVector(direction)
    return movementVector(private.convertRelativeDirectionToHeading(direction))
end

function movementVector(heading)
    return movementVectors[heading]
end

function moveTo(coord)
    while not private.compareCoords(location(), coord) do
        private.moveToward(coord)
    end
end

function getHeading(coordinate, sign)
    if sign == POSITIVE then
        if coordinate == X then
            return EAST
        elseif coordinate == Z then
            return SOUTH
        end
    elseif sign == NEGATIVE then
        if coordinate == X then
            return WEST
        elseif coordinate == Z then
            return NORTH
        end
    else
        error("invalid direction: " .. tostring(sign))
    end

    error("invalid coordinate: " .. tostring(sign))
end

function changeHeading(heading)
    local delta = (state.heading - heading) % 4
    if delta == 1 then
        turnLeft()
    elseif delta == 2 then
        turnRight()
        turnRight()
    elseif delta == 3 then
        turnRight()
    end
end

function turnLeft()
    turtle.turnLeft()
    state.heading = (state.heading - 1) % 4
    private.saveState()
end

function turnRight()
    turtle.turnRight()
    state.heading = (state.heading + 1) % 4
    private.saveState()
end

function move(direction)
    if direction ~= FORWARD and direction ~= UP and direction ~= DOWN then
        error("move: direction not supported - " .. tostring(direction))
    end

    local distanceToHome = private.homeDistance(private.nextCoordRelative(direction))
    if not private.refuel(distanceToHome) then
        returnItems()
    end

    while not private.move(direction) do
        if detect(direction) then
            if not dig(direction) then
                return false
            end
        else
            if attack(direction) then
                while suck(direction) do
                end
                if not private.hasEmptySlot() then
                    returnItems()
                end
            end
        end
    end
    return true
end

function detect(direction)
    if direction == FORWARD then
        return turtle.detect()
    elseif direction == UP then
        return turtle.detectUp()
    elseif direction == DOWN then
        return turtle.detectDown()
    else
        error("detect: invalid direction - " .. tostring(direction))
    end
end

function attack(direction)
    if direction == FORWARD then
        return turtle.attack()
    elseif direction == UP then
        return turtle.attackUp()
    elseif direction == DOWN then
        return turtle.attackDown()
    else
        error("attack: invalid direction - " .. tostring(direction))
    end
end

function suck(direction)
    if direction == FORWARD then
        return turtle.suck()
    elseif direction == UP then
        return turtle.suckUp()
    elseif direction == DOWN then
        return turtle.suckDown()
    else
        error("suck: invalid direction - " .. tostring(direction))
    end
end

function drop(direction)
    if direction == FORWARD then
        return turtle.drop()
    elseif direction == UP then
        return turtle.dropUp()
    elseif direction == DOWN then
        return turtle.dropDown()
    else
        error("drop: invalid direction - " .. tostring(direction))
    end
end

function dig(direction)
    if not private.hasEmptySlot() then
        returnItems()
    end

    if direction == FORWARD then
        return turtle.dig()
    elseif direction == UP then
        return turtle.digUp()
    elseif direction == DOWN then
        return turtle.digDown()
    else
        error("dig: invalid direction - " .. tostring(direction))
    end
end

function forward()
    return move(FORWARD)
end

function up()
    return move(UP)
end

function down()
    return move(DOWN)
end

function private.hasEmptySlot()
    for n=16,1,-1 do
        if turtle.getItemCount(n) == 0 then
            return true
        end
    end
    return false
end

function private.refuel(minimumFuel)
    -- TODO: keep track of fuel usage
    if turtle.getFuelLevel() <= minimumFuel then
        for n=1,16 do
            turtle.select(n)
            while turtle.refuel(1) do
                if turtle.getFuelLevel() >= minimumFuel then
                    return true
                end
            end
        end
        return false
    end
    return true
end

function private.homeDistance(coord)
    return private.taxiDistance(home(), coord)
end

function private.taxiDistance(coord1, coord2)
    local sum = 0
    for n=1,3 do
        sum = sum + math.abs(coord1[n] - coord2[n])
    end
    return sum
end

function private.nextCoordRelative(relativeDirection)
    local heading = private.convertRelativeDirectionToHeading(relativeDirection)
    return private.nextCoordAbsolute(heading)
end

function private.nextCoordAbsolute(heading)
    return addVector(location(), movementVectors[heading])
end

function private.convertRelativeDirectionToHeading(relativeDirection)
    if relativeDirection == UP then
        return UP
    elseif relativeDirection == DOWN then
        return DOWN
    else
        return (relativeDirection + state.heading) % 4
    end
end

function private.move(direction)
    private.refuel(1)
    local result
    if direction == FORWARD then
        result = turtle.forward()
    elseif direction == BACKWARD then
        result = turtle.back()
    elseif direction == UP then
        result = turtle.up()
    elseif direction == DOWN then
        result = turtle.down()
    else
        error("move: invalid direction - " .. tostring(direction))
    end
    if result then
        state.location = private.nextCoordRelative(direction)
        private.saveState()
    end
    return result
end

local returningItems = false
function returnItems(callback)
    if returningItems then
        return
    end

    returningItems = true

    local originalLocation, originalHeading = location()

    local homeCoord, homeHeading, homeBlock = home()
    moveTo(homeCoord)
    changeHeading((homeHeading + 2) % 4)

    local success, blockDetail = turtle.inspect()
    local actualHomeBlock
    if not success then
        actualHomeBlock = nil
    else
        actualHomeBlock = getBlockName(blockDetail)
    end

    if (homeBlock ~= actualHomeBlock) then
        error("I don't seem to be at home. Aborting!")
    end

    dumpItems(FORWARD, true)

    if callback then
        callback()
    end

    local distance = private.taxiDistance(location(), originalLocation)
    while not private.refuel(distance * 2) do
        log("waiting for fuel")
        os.pullEvent("turtle_inventory")
    end

    returningItems = false

    moveTo(originalLocation)
    changeHeading(originalHeading)
end

function dumpItems(direction, keepFirstFuel)
    local keptFuel = false
    for n=1,16 do
        turtle.select(n)
        if turtle.getItemCount(n) > 0 then
            if keepFirstFuel and not keptFuel and turtle.refuel(0) then
                keptFuel = true
            else
                local printed = false
                while not drop(direction) do
                    if not printed then
                        log("Chest is full, waiting")
                        printed = true
                    end
                    sleep(10)
                end
            end
        end
    end
end

function private.isHorizontal(heading)
    return heading == SOUTH or heading == WEST or heading == NORTH or heading == EAST
end

function private.attemptMove(dest, heading, digIfNeeded)
    local coordAfterMove = private.nextCoordAbsolute(heading)

    if private.taxiDistance(coordAfterMove, dest) < private.taxiDistance(location(), dest) then
        if private.isHorizontal(heading) then
            changeHeading(heading)
            if digIfNeeded or not detect(FORWARD) then
                if move(FORWARD) then
                    return true
                end
            end
        else
            if digIfNeeded or not detect(heading) then
                if move(heading) then
                    return true
                end
            end
        end
    end

    return false
end

function private.moveToward(dest)
    local tryMove = function(digIfNeeded)
        local directions = private.shuffle({FORWARD, UP, DOWN})

        for n=1,3 do
            local direction = directions[n]
            local heading = private.convertRelativeDirectionToHeading(direction)

            if private.attemptMove(dest, heading, digIfNeeded) then
                return true
            end
        end

        -- grab the backward heading before we potentially turn below
        local backwardHeading = private.convertRelativeDirectionToHeading(BACKWARD)

        directions = {RIGHT, LEFT}
        for n=1,2 do
            local direction = directions[n]
            local heading = private.convertRelativeDirectionToHeading(direction)
            if private.attemptMove(dest, heading, digIfNeeded) then
                return true
            end
        end

        do
            if private.attemptMove(dest, backwardHeading, digIfNeeded) then
                return true
            end
        end
        return false
    end

    -- first try to move without digging. If that's not possible, then try again
    -- with digging
    if not tryMove(false) then
        tryMove(true)
    end
end

function private.shuffle(table)
    for n=1,#table do
        local index = math.random(n,#table)
        local val = table[n]
        table[n] = table[index]
        table[index] = val
    end
    return table
end

function private.readLocation()
    if not fs.exists(LOCATION_FILE) then
        return {0, 0, 0}, SOUTH
    end

    local f, msg = fs.open(LOCATION_FILE, "r")
    if not f then
        error(msg)
    end

    local x = tonumber(f.readLine())
    local y = tonumber(f.readLine())
    local z = tonumber(f.readLine())
    local heading = tonumber(f.readLine())
    f.close()

    return {x, y, z}, heading
end

function private.saveState()
    if not IN_GAME then
        return
    end
    local f, msg = fs.open(LOCATION_FILE, "w")
    if not f then
        error(msg)
    end

    f.writeLine(tostring(state.location[X]))
    f.writeLine(tostring(state.location[Y]))
    f.writeLine(tostring(state.location[Z]))
    f.writeLine(tostring(state.heading))
    f.close()
end

function setLocation(coord, heading)
    state.location = coord
    state.heading = heading
    private.saveState()
end

function getBlockName(blockDetail)
    if blockDetail.damage ~= nil then
        return blockDetail.name .. ":" .. tostring(blockDetail.damage)
    end
    return blockDetail.name .. ":"
end

function parseDirection(direction)
    direction = direction:upper()
    if direction == "UP" then
        return UP
    elseif direction == "DOWN" then
        return DOWN
    elseif direction == "FORWARD" then
        return FORWARD
    elseif direction == "BACKWARD" then
        return BACKWARD
    elseif direction == "LEFT" then
        return LEFT
    elseif direction == "RIGHT" then
        return RIGHT
    else
        return nil
    end
end

state = {}
state.location, state.heading = private.readLocation()