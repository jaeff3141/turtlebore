args = { ... }

os.loadAPI("utils")

if #args ~= 3 then
    print("Usage: transfer input_direction output_direction delay_seconds")
    return
end

acceptedDirections = {}
acceptedDirections[UP] = true
acceptedDirections[DOWN] = true
acceptedDirections[FORWARD] = true

input = utils.parseDirection(args[1])
output = utils.parseDirection(args[2])
delay = tonumber(args[3])

if input == nil or acceptedDirections[input] == nil then
    print(string.format("%s is not a valid direction. must be one of up, down or forward", input))
    return
end

if output == nil or acceptedDirections[output] == nil then
    print(string.format("%s is not a valid direction. must be one of up, down or forward", output))
    return
end

if delay == nil then
    print(string.format("%s is not a valid number"))
    return
end

while true do
    local didSomething = false

    while utils.suck(input) do
        utils.drop(output)
    end

    sleep(delay)
end



