os.loadAPI("utils")

while true do
    turtle.select(1)
    if utils.suck(DOWN) then
        local count = turtle.getItemCount()
        for i=1,count do
            turtle.placeUp()
            turtle.digUp()
        end
        utils.dumpItems(FORWARD, false)
    else
        sleep(10)
    end
end
