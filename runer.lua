-- up - chest with wand, items to runic alter
-- forward - runic alter
-- down - interface
-- left - comparator from runic altar

function inventoryEmpty()
    for n=1,16 do
        if turtle.getItemCount(n) > 0 then
            return false
        end
    end
    return true
end

function getInput(side)
    local value = redstone.getAnalogInput(side)
    print(string.format("got redstone=%d", value))
    return value
end

while true do
    print("Waiting for inventory.")
    os.pullEvent("turtle_inventory")

    print("Inventory changed. We're up!")

    if not inventoryEmpty() then
        print("Yep, got something!")

        -- grab the wand. This ensures that the interface can't push any more items
        turtle.select(16)
        turtle.suckUp()

        for n=1,15 do
            if turtle.getItemCount(n) > 0 then
                print(string.format("got an item in slot %d", n))
                turtle.select(n)
                if not turtle.dropUp() then
                    print("hmm. couldn't drop the items")
                end
            end
        end

        print("Wanding")
        turtle.select(16)
        turtle.place()

        while getInput("right") == 0 do
            print("Wanding")
            turtle.place()
            sleep(.1)
        end
        print("got initial redstone signal. waiting for completion")


        while true do
            os.pullEvent("redstone")
            if getInput("right") >= 2 then
                while getInput("right") >= 2 do
                    print("wanding for completion")
                    turtle.place()
                    sleep(.1)
                end
                break
            end
        end

        print("All done")
        turtle.dropUp()
    else
        print("Aw crud, there's nothing to do.")
    end
end

