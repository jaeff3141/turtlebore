-- down = fertilizer
-- forward = interface with scrap

while true do
    print("trying to clean inventory")
    local full = false
    for n = 1,16 do
        if turtle.getItemCount(n) > 0 then
            print("found an item")
            turtle.select(n)
            if not turtle.dropDown() then
                print("chest is full")
                full = true
                break
            end
        end
    end

    while not full do
        turtle.select(1)
        turtle.suckDown()
        local itemCount = turtle.getItemCount()
        print(string.format("got %d items", itemCount))
        if itemCount > 0 then
            turtle.select(2)
            turtle.suck(itemCount)
            turtle.select(3)
            turtle.suck(itemCount)
            turtle.select(4)

            while turtle.getItemCount(1) > 0 do
                turtle.craft()

                if turtle.getItemCount() == 0 then
                    break
                end
                if not turtle.dropDown() then
                    print("chest is full")
                    full = true
                    break
                end
            end

            for n=1,16 do
                if turtle.getItemCount(n) > 0 then
                    turtle.select(n)
                    if turtle.getItemDetail()["name"] == "IC2:itemScrap" then
                        print("had some scrap leftover")
                        turtle.drop()
                    elseif turtle.getItemDetail()["name"] == "IC2:itemFertilizer" then
                        print("clearing out fertilizer")
                        if not turtle.dropDown() then
                            print("chest is full")
                            full = true
                            break
                        end
                    end
                end
            end
        else
            print("Couldn't suck any fertilizer. Is it empty?")
            break
        end
    end

    sleep(10)
end