os.loadAPI("utils")

while true do
    while true do
        local didSomething = false
        for n=1,16 do
            turtle.select(n)
            if turtle.getItemCount() > 0 then
                didSomething = true
                local blockDetails = turtle.getItemDetail()
                local blockName = utils.getBlockName(blockDetails)
                if blockName == "TConstruct:strangeFood:0" then -- blue slime balls
                    turtle.dropDown()
                else
                    turtle.drop()
                end
            end
        end

        if not didSomething then
            break
        end
    end

    os.pullEvent("turtle_inventory")
end
