IN_GAME = true
if turtle == nil then
    IN_GAME = false
end

if not IN_GAME then
    function os.loadAPI(name)
        local loadedFile,msg = loadfile(name .. ".lua")
        if not loadedFile then
            error(msg)
        end

        local sandbox = {}
        getfenv(1)[name] = sandbox
        setmetatable(sandbox, {__index = function(table, key) return getfenv(1)[key] end })
        setfenv(loadedFile, sandbox)
        return loadedFile()
    end
end
os.loadAPI("utils")

args = { ... }

BORE_LOG = "bore_log"
HOME_COMMAND = ".gohome"
SAVED_STATE = ".bore"

-- TODO: if * is used for something other than depth, we should change the dig order to accomodate
function usage()
    print("Usage: bore [width] [length] [depth]")
    print("       bore [startForward] [startSide] [startDepth] [width] [length] [depth]")
    print("       bore continue")
    print("Hints: width is front/back size, length is right/left size")
    print("       forward, right and up are positive")
    print("       you can use * or -* as shorthand to go as far as possible")
end

utils.init(_G)

function getStartDepth(state)
    local startY = state.startCoord[Y]
    local endY = state.endCoord[Y]

    if math.abs(startY - endY) == 1 then
        local currentY = utils.location()[Y]
        if currentY <= startY and currentY <= endY then
            return math.min(startY, endY)
        else
            return math.max(startY, endY)
        end
    end

    if startY == endY then
        -- The bore is only 1 block deep
        return startY
    elseif startY < endY then
        -- we're boring up, we can start one block up
        return startY + 1
    else
        -- we're boring down, we can start one block down
        return startY - 1
    end
end

function start(startForward, startSide, startDepth, widthForward, widthSide, depth)
    local state = {}

    startForward = tonumber(startForward)
    startSide = tonumber(startSide)
    startDepth = tonumber(startDepth)
    if startForward == nil or startSide == nil or startDepth == nil then
        return false
    end

    local startCoord = utils.location()
    startCoord = utils.addVector(startCoord,
        utils.multiplyVector(utils.relativeMovementVector(FORWARD), startForward))
    startCoord = utils.addVector(startCoord,
        utils.multiplyVector(utils.relativeMovementVector(RIGHT), startSide))
    startCoord = utils.addVector(startCoord,
        utils.multiplyVector(utils.relativeMovementVector(UP), startDepth))
    state.startCoord = startCoord

    widthForward = parseEnd(widthForward)
    widthSide = parseEnd(widthSide)
    depth = parseEnd(depth)
    if widthForward == nil or widthSide == nil or depth == nil then
        return false
    end

    local endCoord = startCoord
    endCoord = utils.addVector(endCoord,
        utils.multiplyVector(utils.relativeMovementVector(FORWARD), widthForward))
    endCoord = utils.addVector(endCoord,
        utils.multiplyVector(utils.relativeMovementVector(RIGHT), widthSide))
    endCoord = utils.addVector(endCoord,
        utils.multiplyVector(utils.relativeMovementVector(UP), depth))
    state.endCoord = endCoord

    state.direction = {}
    for n=1,3 do
        if state.startCoord[n] < state.endCoord[n] then
            state.direction[n] = POSITIVE
        else
            state.direction[n] = NEGATIVE
        end
    end

    state.digAxis = {Z, X}

    utils.moveTo({state.startCoord[X],
        getStartDepth(state),
        state.startCoord[Z]})

    fs.delete(HOME_COMMAND)

    logBore()
    saveState(state)
    bore(state)
    return true
end

function parseEnd(endVal, adjust)
    if adjust == nil then
        adjust = true
    end
    if endVal == "*" then
        return math.huge
    elseif endVal == "-*" then
        return -1 * math.huge
    else
        local val = tonumber(endVal)
        if adjust and val > 0 then
            return val - 1
        elseif adjust and val < 0 then
            return val + 1
        else
            return val
        end
    end
end

function continue()
    local state, initialLocation, initialHeading = loadState()
    if not state then
        print("Couldn't load state to continue")
        return false
    end
    utils.moveTo(initialLocation)
    utils.changeHeading(initialHeading)
    bore(state)
    return true
end

function logBore()
    local f, msg = fs.open(BORE_LOG, "a")
    if not f then
        error(msg)
    end

    f.writeLine(table.concat(args, " "))
    f.close()
end

function saveState(state)
    if not IN_GAME then
        return
    end

    local f, msg = fs.open(SAVED_STATE, "w")
    if not f then
        error(msg)
    end

    local formatEndCoord = function(endValue)
        -- tostring(math.huge) is nil, unfortuntely
        if endValue == math.huge then
            return "*"
        elseif endValue == (-1 * math.huge) then
            return "-*"
        else
            return tostring(endValue)
        end
    end

    f.writeLine(tostring(state.startCoord[1]))
    f.writeLine(tostring(state.startCoord[2]))
    f.writeLine(tostring(state.startCoord[3]))

    f.writeLine(formatEndCoord(state.endCoord[1]))
    f.writeLine(formatEndCoord(state.endCoord[2]))
    f.writeLine(formatEndCoord(state.endCoord[3]))

    f.writeLine(tostring(state.direction[1]))
    f.writeLine(tostring(state.direction[2]))
    f.writeLine(tostring(state.direction[3]))

    f.writeLine(tostring(state.digAxis[1]))
    f.writeLine(tostring(state.digAxis[2]))

    local location, heading = utils.location()
    f.writeLine(tostring(location[1]))
    f.writeLine(tostring(location[2]))
    f.writeLine(tostring(location[3]))
    f.writeLine(tostring(heading))
    f.close()
end

function loadState()
    local state = {}

    if not fs.exists(SAVED_STATE) then
        return false
    end

    local f, msg = fs.open(SAVED_STATE, "r")
    if not f then
        error(msg)
    end

    state.startCoord = {
        tonumber(f.readLine()),
        tonumber(f.readLine()),
        tonumber(f.readLine())
    }

    state.endCoord = {
        parseEnd(f.readLine(), false),
        parseEnd(f.readLine(), false),
        parseEnd(f.readLine(), false)
    }

    state.direction = {
        tonumber(f.readLine()),
        tonumber(f.readLine()),
        tonumber(f.readLine())
    }

    state.digAxis = {
        tonumber(f.readLine()),
        tonumber(f.readLine())
    }

    local initialLocation = {
        tonumber(f.readLine()),
        tonumber(f.readLine()),
        tonumber(f.readLine())
    }
    local heading = tonumber(f.readLine())
    f.close()
    return state, initialLocation, heading
end

function bore(state)
    repeat
        repeat
            while digNext(state, state.digAxis[1]) do
            end
            state.direction[state.digAxis[1]] = state.direction[state.digAxis[1]] * -1
        until not digNext(state, state.digAxis[2])
        state.direction[state.digAxis[2]] = state.direction[state.digAxis[2]] * -1

        -- swap the first and second dig axis, so that we don't have to do a double turn
        -- after moving vertically
        local tmp = state.digAxis[1]
        state.digAxis[1] = state.digAxis[2]
        state.digAxis[2] = tmp
    until not digNext(state, Y)

    digUp(state)
    digDown(state)
end

function digNext(state, axis)
    if fs.exists(HOME_COMMAND) then
        utils.returnItems(function()
            while fs.exists(HOME_COMMAND) do
                sleep(5)
            end
        end)
    end

    if axis == X or axis == Z then
        return digHorizontal(state, axis)
    elseif axis == Y then
        return digVertical(state)
    else
        error("Invalid axis: " .. tostring(axis))
    end
end

function digHorizontal(state, axis)
    if not blockInBounds(state, getNextCoordinate(state, axis, 1)) then
        return false
    end
    digUp(state)
    digDown(state)
    utils.changeHeading(utils.getHeading(axis, state.direction[axis]))
    utils.dig(FORWARD)
    if utils.forward() then
        saveState(state)
        return true
    end
    return false
end

function digVertical(state)
    digUp(state)
    digDown(state)
    local moved = false
    for n=1,3 do
        -- stop 1 block from the bottom/top, no need to go all the way down/up
        if not blockInBounds(state, getNextCoordinate(state, Y, 2)) then
            break
        end

        if state.direction[Y] == 1 then
            if not utils.up() then
                break
            end
            moved = true
        else
            if not utils.down() then
                break
            end
            moved = true
        end
    end
    if moved then
        saveState(state)
        return true
    end
    return false
end

function digUp(state)
    if not blockInBounds(state, getCoordinateInHeading(UP)) then
        return
    end

    utils.dig(UP)
end

function digDown(state)
    if not blockInBounds(state, getCoordinateInHeading(DOWN)) then
        return
    end

    utils.dig(DOWN)
end

function getBounds(state, axis)
    local val1 = state.startCoord[axis]
    local val2 = state.endCoord[axis]

    if val1 < val2 then
        return val1, val2
    else
        return val2, val1
    end
end

function blockInBounds(state, coordinate)
    for n=1,3 do
        local min, max = getBounds(state, n)
        local coordVal = coordinate[n]
        if coordVal < min or coordVal > max then
            return false
        end
    end
    return true
end

function getNextCoordinate(state, axis, distance)
    local location = utils.location()
    location[axis] = location[axis] + (distance * state.direction[axis])
    return location
end

function getCoordinateInHeading(heading)
    return utils.addVector(utils.location(), utils.movementVector(heading))
end

if #args == 3 then
    if not start(0, 0, 0, unpack(args)) then
        usage()
        return
    end
elseif #args == 6 then
    if not start(unpack(args)) then
        usage()
        return
    end
elseif #args == 1 then
    if args[1] == "continue" then
        continue()
    else
        usage()
        return
    end
else
    usage()
    return
end

if (IN_GAME) then
    fs.delete(SAVED_STATE)
end

local home, heading = utils.home()
utils.moveTo(home)
utils.changeHeading((heading + 2) % 4)
utils.dumpItems(FORWARD, false)
utils.changeHeading(heading)