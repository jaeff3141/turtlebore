args = { ... }

os.loadAPI("utils")

-- forward = input
-- down = temp space
-- up = permanent storage

SLOTS = {1, 2, 3, 5, 6, 7, 9, 10, 11}

function spreadItemsAndCraft()
    local itemCount = turtle.getItemCount()
    local spreadCount = math.floor(itemCount / 9)

    for n=2,9 do
        turtle.transferTo(SLOTS[n], spreadCount)
    end

    turtle.select(13)
    return turtle.craft()
end

function clearInventory(tempDirection)
    for n=1,16 do
        if turtle.getItemCount(n) > 0 then
            turtle.select(n)
            utils.drop(tempDirection)
        end
    end
end

function suckCobble(n)
    turtle.select(n)
    while not utils.suck(FORWARD) do
        sleep(10)
    end
end

function recompress()
    local sourceDir = UP
    local tempDir = DOWN
    clearInventory(tempDir)
    while true do
        local compressedSomething = false
        while true do
            turtle.select(1)
            if not utils.suck(sourceDir) then
                break
            end

            if turtle.getItemCount() >= 9 then
                spreadItemsAndCraft()
                compressedSomething = true
            end
            clearInventory(tempDir)
        end

        if not compressedSomething then
            break
        end

        local tmp = sourceDir
        sourceDir = tempDir
        tempDir = tmp
    end

    if tempDir ~= UP then
        -- move everything back to the top chest
        turtle.select(1)
        while utils.suck(DOWN) do
            utils.drop(UP)
        end
    end
end

function compress()
    clearInventory(UP)
    while true do
        for n=1,9 do
            suckCobble(SLOTS[n])
        end

        turtle.select(13)
        turtle.craft()
        if not utils.drop(UP) then
            recompress()
        end
    end
end

while true do
    print("compressing...")
    compress()
    print("waiting...")
    sleep(60)
end