function nsh()
    shell.run("nsh host")
end

function continueBore()
    if fs.exists(".bore") then
        shell.run("bore continue")
    end
end

if fs.exists(".bore") then
    print("Resuming bore operation in 10 seconds")
    print("Press A to abort resumption")

    local timerId = os.startTimer(10)

    while true do
        local event, arg = os.pullEvent()
        if event == "key" and arg == keys.a then
            print("Resumption aborted")
            continueBore = function() end
            break
        elseif event == "timer" and arg == timerId then
            print("Resuming bore operation")
            break
        end
    end
end

parallel.waitForAll(nsh, continueBore)

