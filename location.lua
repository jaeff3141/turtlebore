IN_GAME = true
if turtle == nil then
    IN_GAME = false
end

if not IN_GAME then
    function os.loadAPI(name)
        local loadedFile,msg = loadfile(name .. ".lua")
        if not loadedFile then
            error(msg)
        end

        local sandbox = {}
        getfenv(1)[name] = sandbox
        setmetatable(sandbox, {__index = function(table, key) return getfenv(1)[key] end })
        setfenv(loadedFile, sandbox)
        return loadedFile()
    end
end
os.loadAPI("utils")

local args = { ... }

function usage()
    print("Usage: location x y z NORTH|EAST|SOUTH|WEST")
end

if #args ~= 4 then
    usage()
    return
end

x = tonumber(args[1])
if x == nil then
    usage()
    return
end

y = tonumber(args[2])
if y == nil then
    usage()
    return
end

z = tonumber(args[3])
if z == nil then
    usage()
    return
end

heading = args[4]
if heading:upper() == "SOUTH" then
    heading = SOUTH
elseif heading:upper() == "WEST" then
    heading = WEST
elseif heading:upper() == "NORTH" then
    heading = NORTH
elseif heading:upper() == "EAST" then
    heading = EAST
else
    usage()
    return
end

utils.setLocation({x, y, z}, heading)
